CC = gcc

CFLAGS = -Iinclude -I../src -g -O2 -std=c11 -Wall -Wextra -Wpedantic -Wno-unused-parameter
TEST_LDFLAGS = -lcmocka

SOURCES = \
	src/graph.c \
	src/statement.c \
	src/statement_iterator.c \
	src/value_container.c

OBJECTS = $(SOURCES:.c=.o)

TEST_SOURCES := $(SOURCES) tests/test_runner.c
TEST_OBJECTS = $(TEST_SOURCES:.c=.o)

LIBRARY = libsempre.a
TEST_RUNNER = test_runner

all: $(SOURCES) $(LIBRARY)

test: $(TEST_SOURCES) $(TEST_RUNNER)

$(TEST_RUNNER) : $(TEST_OBJECTS)
	$(CC) $(TEST_OBJECTS) $(TEST_LDFLAGS) -o $@

$(LIBRARY) : $(OBJECTS)
	ar rcs $(LIBRARY) $(OBJECTS)

.cpp.o:
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm $(OBJECTS)

