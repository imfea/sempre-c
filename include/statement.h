#ifndef SEMPRE_STATEMENT_H
#define SEMPRE_STATEMENT_H

#include "types.h"

/**
 * Statement by node identifiers
 */
typedef struct Statement
{
    Id object_id;
    Id predicate_id;
    Id subject_id;
} Statement;

#endif /* SEMPRE_STATEMENT_H */
