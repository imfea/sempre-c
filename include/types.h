#ifndef SEMPRE_TYPES_H
#define SEMPRE_TYPES_H

#include <stddef.h>

/**
 * Identifier type
 */
typedef unsigned long Id;

/**
 * Status codes for error handling
 */
typedef enum Status {
    STATUS_OK,
    STATUS_INVALID_ID,
    STATUS_INTEGRITY_ERROR,
    STATUS_MISSING_STATEMENT
} Status;

#endif /* SEMPRE_TYPES_H */
