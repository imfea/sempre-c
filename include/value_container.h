#ifndef SEMPRE_VALUE_CONTAINER_H
#define SEMPRE_VALUE_CONTAINER_H

#include "statement.h"
#include "types.h"

/**
 * Value item for list implementation
 */
typedef struct ValueItem
{
    Id id;
    char* value;
    struct ValueItem* next;
} ValueItem;

/**
 * Map like container for the values
 */
typedef struct ValueContainer
{
    ValueItem* values;
    Id last_id;
} ValueContainer;

/**
 * Initialize the value container.
 */
void init_value_container(ValueContainer* const value_container);

/**
 * Create new value.
 */
Id create_value(ValueContainer* const value_container, const char* const value);

/**
 * Destroy an existing value
 */
Status destroy_value(ValueContainer* const value_container, Id value_id);

/**
 * Get value by identifier.
 * @return NULL on invalid value identifier
 */
char* get_value(const ValueContainer* const value_container, Id value_id);

/**
 * Find the identifier by value.
 */
Id find_value_id(const ValueContainer* const value_container, const char* const value);

/**
 * Count the values of the container.
 */
int count_values(ValueContainer* const value_container);

/**
 * Clear all values.
 */
void clear_values(ValueContainer* const value_container);

#endif /* SEMPRE_VALUE_CONTAINER_H */
