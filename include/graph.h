#ifndef SEMPRE_GRAPH_H
#define SEMPRE_GRAPH_H

#include "statement.h"
#include "statement_iterator.h"
#include "types.h"
#include "value_container.h"

#include <stdbool.h>

/**
 * Directed named graph
 */
typedef struct Graph
{
    StatementItem* statements;
    ValueContainer value_container;
} Graph;

/**
 * Initialize an empty graph.
 */
void init_graph(Graph* const graph);

/**
 * Release the allocated memory of the graph.
 */
void release_graph(Graph* const graph);

/**
 * Save graph data to the filesystem.
 */
void save_graph(const Graph* const graph, const char* const path);

/**
 * Load graph data from the filesystem.
 */
void load_graph(Graph* const graph, const char* const path);

/**
 * Create new node.
 */
Id create_node(Graph* const graph, const char* const value);

/**
 * Destroy the given node.
 * Only the isolated nodes can be destroyed.
 */
Status destroy_node(Graph* const graph, Id node_id);

/**
 * Find the unique node identifier by value.
 * Returns 0 when the value does not exist.
 */
Id find_node_id(Graph* const graph, const char* const value);

/**
 * Get the value of the given node.
 */
char* get_node_value(const Graph* const graph, Id node_id);

/**
 * Create a statement from values.
 */
Status create_statement_from_values(Graph* const graph,
                                    const char* const subject,
                                    const char* const predicate,
                                    const char* const object);

/**
 * Create a statement from subject id and values.
 */
Status create_statement_for_subject(Graph* const graph,
                                    Id subject_id,
                                    const char* const predicate,
                                    const char* const object);

/**
 * Create a statement from subject and object id and predicate value.
 */
Status create_statement_by_predicate(Graph* const graph,
                                     Id subject_id,
                                     const char* const predicate,
                                     Id object_id);

/**
 * Create a new statement.
 * The statements cannot be duplicated. An existing statement has not cause problem on creation.
 */
Status create_statement(Graph* const graph, const Statement* const statement);

/**
 * Destroy the statement.
 */
Status destroy_statement_by_values(Graph* const graph,
                                   const char* const subject,
                                   const char* const predicate,
                                   const char* const object);

/**
 * Destroy the statement.
 */
Status destroy_statement(Graph* const graph, const Statement* const statement);

/**
 * Check that the statement is exists.
 */
bool has_statement(Graph* const graph, const Statement* const statement);

/**
 * Check that the statement is exists.
 */
bool has_statement_by_values(Graph* const graph,
                             const char* const subject,
                             const char* const predicate,
                             const char* const object);

/**
 * Find statements by object identifier.
 */
Status find_statements(Graph* const graph, Id subject_id, StatementIterator* statement_iterator);

/**
 * Check that the node has the given predicate.
 */
bool has_predicate_by_id(const Graph* const graph, Id node_id, Id predicate_id);

/**
 * Check that the node has the given predicate.
 */
bool has_predicate(const Graph* const graph, Id subject_id, const char* const predicate);

/**
 * Check that the node has the given predicate.
 */
bool has_predicate_by_values(const Graph* const graph,
                             const char* const subject,
                             const char* const predicate);

/**
 * Count the nodes of the graph.
 */
int count_nodes(Graph* const graph);

/**
 * Count the statements of the graph.
 */
int count_statements(Graph* const graph);

#endif /* SEMPRE_GRAPH_H */
