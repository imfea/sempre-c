#ifndef SEMPRE_STATEMENT_ITERATOR_H
#define SEMPRE_STATEMENT_ITERATOR_H

#include "statement.h"

#include <stdbool.h>

/**
 * Statement item for linked-list
 */
typedef struct StatementItem
{
    Statement statement;
    struct StatementItem* next;
} StatementItem;

/**
 * Statement iterator
 */
typedef struct StatementIterator
{
    StatementItem* items;
    StatementItem* current_item;
    Statement* statement;
} StatementIterator;

/**
 * Initialize an empty iterator.
 */
void init_iterator(StatementIterator* const statement_iterator);

/**
 * Append new element to the end of the statement list.
 * It assumes that the current item is the last element (after and before the operation).
 */
void append_statement(StatementIterator* const statement_iterator, const Statement* const statement);

/**
 * Set the iterator before the first element.
 */
void reset_iterator(StatementIterator* const statement_iterator);

/**
 * Advance the iterator to the next statement.
 */
void advance_iterator(StatementIterator* const statement_iterator);

/**
 * Check that is there a next statement
 * @return true, when there is a next statement, else false
 */
bool has_next_statement(const StatementIterator* const statement_iterator);

/**
 * Release the allocated statements.
 */
void release_iterator(StatementIterator* const statement_iterator);

#endif /* SEMPRE_STATEMENT_ITERATOR_H */
