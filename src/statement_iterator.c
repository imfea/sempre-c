#include "statement_iterator.h"

#include <stdlib.h>

void init_iterator(StatementIterator* const statement_iterator)
{
    statement_iterator->items = NULL;
    statement_iterator->current_item = NULL;
    statement_iterator->statement = NULL;
}

void append_statement(StatementIterator* const statement_iterator, const Statement* const statement)
{
    StatementItem* statement_item;

    statement_item = (StatementItem*)malloc(sizeof(StatementItem));
    statement_item->statement.subject_id = statement->subject_id;
    statement_item->statement.predicate_id = statement->predicate_id;
    statement_item->statement.object_id = statement->object_id;
    statement_item->next = NULL;
    if (statement_iterator->items == NULL) {
        statement_iterator->items = statement_item;
    }
    else {
        statement_iterator->current_item->next = statement_item;
    }
    statement_iterator->current_item = statement_item;
    statement_iterator->statement = &(statement_item->statement);
}

void reset_iterator(StatementIterator* const statement_iterator)
{
    if (statement_iterator->items != NULL) {
        statement_iterator->current_item = statement_iterator->items;
        statement_iterator->statement = &(statement_iterator->current_item->statement);
    }
}

void advance_iterator(StatementIterator* const statement_iterator)
{
    if (statement_iterator->current_item != NULL) {
        statement_iterator->current_item = statement_iterator->current_item->next;
        if (statement_iterator->current_item != NULL) {
            statement_iterator->statement = &(statement_iterator->current_item->statement);
        }
    }
}

bool has_next_statement(const StatementIterator* const statement_iterator)
{
    if (statement_iterator->current_item != NULL && statement_iterator->current_item->next != NULL) {
        return true;
    }
    return false;
}

void release_iterator(StatementIterator* const statement_iterator)
{
    StatementItem* item;
    StatementItem* next_item;

    item = statement_iterator->items;
    while (item != NULL) {
        next_item = item->next;
        free(item);
        item = next_item;
    }
    statement_iterator->items = NULL;
    statement_iterator->current_item = NULL;
    statement_iterator->statement = NULL;
}
