#include "graph.h"

#include <stdlib.h>

void init_graph(Graph* const graph)
{
    graph->statements = NULL;
    init_value_container(&graph->value_container);
}

void release_graph(Graph * const graph)
{
    StatementItem* item;
    StatementItem* next_item;

    item = graph->statements;
    while (item != NULL) {
        next_item = item->next;
        free(item);
        item = next_item;
    }
    clear_values(&graph->value_container);
}

void save_graph(const Graph* const graph, const char* const path)
{
}

void load_graph(Graph* const graph, const char* const path)
{
}

Id create_node(Graph* const graph, const char* value)
{
    Id node_id;

    node_id = create_value(&graph->value_container, value);
    return node_id;
}

Status destroy_node(Graph* const graph, Id node_id)
{
    Status status;
    StatementItem* item;

    item = graph->statements;
    while (item != NULL) {
        if (item->statement.subject_id == node_id
            || item->statement.predicate_id == node_id
            || item->statement.object_id == node_id)
        {
            return STATUS_INTEGRITY_ERROR;
        }
        item = item->next;
    }
    status = destroy_value(&graph->value_container, node_id);
    return status;
}

Id find_node_id(Graph* const graph, const char* const value)
{
    Id node_id;

    node_id = find_value_id(&graph->value_container, value);
    return node_id;
}

char* get_node_value(const Graph* const graph, Id node_id)
{
    char* value;

    value = get_value(&graph->value_container, node_id);
    return value;
}

Status create_statement_from_values(Graph* const graph,
                                    const char* const subject,
                                    const char* const predicate,
                                    const char* const object)
{
    Statement statement;
    Status status;

    statement.subject_id = create_value(&graph->value_container, subject);
    statement.predicate_id = create_value(&graph->value_container, predicate);
    statement.object_id = create_value(&graph->value_container, object);
    status = create_statement(graph, &statement);
    return status;
}

Status create_statement_for_subject(Graph* const graph,
                                    Id subject_id,
                                    const char* const predicate,
                                    const char* const object)
{
    Statement statement;
    Status status;

    statement.subject_id = subject_id;
    statement.predicate_id = create_value(&graph->value_container, predicate);
    statement.object_id = create_value(&graph->value_container, object);
    status = create_statement(graph, &statement);
    return status;
}

Status create_statement_by_predicate(Graph* const graph,
                                     Id subject_id,
                                     const char* const predicate,
                                     Id object_id)
{
    Statement statement;
    Status status;

    statement.subject_id = subject_id;
    statement.predicate_id = create_value(&graph->value_container, predicate);
    statement.object_id = object_id;
    status = create_statement(graph, &statement);
    return status;
}

Status create_statement(Graph* const graph, const Statement* const statement)
{
    StatementItem* new_item;

    if (has_statement(graph, statement) == true) {
        return STATUS_OK;
    }
    new_item = (StatementItem*)malloc(sizeof(StatementItem));
    new_item->statement.subject_id = statement->subject_id;
    new_item->statement.predicate_id = statement->predicate_id;
    new_item->statement.object_id = statement->object_id;
    new_item->next = graph->statements;
    graph->statements = new_item;
    return STATUS_OK;
}

Status destroy_statement_by_values(Graph* const graph,
                                   const char* const subject,
                                   const char* const predicate,
                                   const char* const object)
{
    Statement statement;
    Status status;

    statement.subject_id = find_value_id(&graph->value_container, subject);
    if (statement.subject_id == 0) {
        return STATUS_MISSING_STATEMENT;
    }
    statement.predicate_id = find_value_id(&graph->value_container, predicate);
    if (statement.predicate_id == 0) {
        return STATUS_MISSING_STATEMENT;
    }
    statement.object_id = find_value_id(&graph->value_container, object);
    if (statement.object_id == 0) {
        return STATUS_MISSING_STATEMENT;
    }
    status = destroy_statement(graph, &statement);
    return status;
}

Status destroy_statement(Graph* const graph, const Statement* const statement)
{
    StatementItem* item;
    StatementItem* prev_item;

    item = graph->statements;
    prev_item = NULL;
    while (item != NULL) {
        if (item->statement.subject_id == statement->subject_id
            && item->statement.predicate_id == statement->predicate_id
            && item->statement.object_id == statement->object_id)
        {
            if (prev_item == NULL) {
                graph->statements = item->next;
            }
            else {
                prev_item->next = item->next;
            }
            free(item);
            return STATUS_OK;
        }
        prev_item = item;
        item = item->next;
    }
    return STATUS_MISSING_STATEMENT;
}

bool has_statement(Graph* const graph, const Statement* const statement)
{
    StatementItem* item;

    item = graph->statements;
    while (item != NULL) {
        if (item->statement.subject_id == statement->subject_id
            && item->statement.predicate_id == statement->predicate_id
            && item->statement.object_id == statement->object_id)
        {
            return true;
        }
        item = item->next;
    }
    return false;
}

bool has_statement_by_values(Graph* const graph,
                             const char* const subject,
                             const char* const predicate,
                             const char* const object)
{
    Statement statement;
    bool result;

    statement.subject_id = find_value_id(&graph->value_container, subject);
    if (statement.subject_id == 0) {
        return false;
    }
    statement.predicate_id = find_value_id(&graph->value_container, predicate);
    if (statement.predicate_id == 0) {
        return false;
    }
    statement.object_id = find_value_id(&graph->value_container, object);
    if (statement.object_id == 0) {
        return false;
    }
    result = has_statement(graph, &statement);
    return result;
}

Status find_statements(Graph* const graph, Id subject_id, StatementIterator* statement_iterator)
{
    StatementItem* item;

    item = graph->statements;
    while (item != NULL) {
        if (item->statement.subject_id == subject_id) {
            append_statement(statement_iterator, &item->statement);
        }
        item = item->next;
    }
    return STATUS_OK;
}

bool has_predicate_by_id(const Graph* const graph, Id node_id, Id predicate_id)
{
    StatementItem* item;

    item = graph->statements;
    while (item != NULL) {
        if (item->statement.subject_id == node_id && item->statement.predicate_id == predicate_id) {
            return true;
        }
        item = item->next;
    }
    return false;
}

bool has_predicate(const Graph* const graph, Id subject_id, const char* const predicate)
{
    Id predicate_id;
    bool result;

    predicate_id = find_value_id(&graph->value_container, predicate);
    if (predicate_id == 0) {
        return false;
    }
    result = has_predicate_by_id(graph, subject_id, predicate_id);
    return result;
}

bool has_predicate_by_values(const Graph* const graph,
                             const char* const subject,
                             const char* const predicate)
{
    Id subject_id;
    Id predicate_id;
    bool result;

    subject_id = find_value_id(&graph->value_container, subject);
    if (subject_id == 0) {
        return false;
    }
    predicate_id = find_value_id(&graph->value_container, predicate);
    if (predicate_id == 0) {
        return false;
    }
    result = has_predicate_by_id(graph, subject_id, predicate_id);
    return result;
}

int count_nodes(Graph* const graph)
{
    int count;

    count = count_values(&graph->value_container);
    return count;
}

int count_statements(Graph* const graph)
{
    StatementItem* item;
    int count;

    count = 0;
    item = graph->statements;
    while (item != NULL) {
        ++count;
        item = item->next;
    }
    return count;
}
