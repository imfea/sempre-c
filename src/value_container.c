#include "value_container.h"

#include <stdlib.h>
#include <string.h>

void init_value_container(ValueContainer* const value_container)
{
    value_container->values = NULL;
    value_container->last_id = 0;
}

Id create_value(ValueContainer* const value_container, const char* const value)
{
    ValueItem* item;
    ValueItem* new_item;

    item = value_container->values;
    while (item != NULL) {
        if (strcmp(item->value, value) == 0) {
            return item->id;
        }
        item = item->next;
    }
    new_item = (ValueItem*)malloc(sizeof(ValueItem));
    ++value_container->last_id;
    new_item->id = value_container->last_id;
    new_item->value = (char*)malloc(strlen(value) + 1);
    strcpy(new_item->value, value);
    new_item->next = value_container->values;
    value_container->values = new_item;
    return value_container->last_id;
}

Status destroy_value(ValueContainer* const value_container, Id value_id)
{
    ValueItem* item;
    ValueItem* prev_item;

    item = value_container->values;
    prev_item = NULL;
    while (item != NULL) {
        if (item->id == value_id) {
            if (prev_item == NULL) {
                value_container->values = item->next;
            }
            else {
                prev_item->next = item->next;
            }
            free(item->value);
            free(item);
            return STATUS_OK;
        }
        prev_item = item;
        item = item->next;
    }
    return STATUS_INVALID_ID;
}

char* get_value(const ValueContainer* const value_container, Id value_id)
{
    ValueItem* item;

    item = value_container->values;
    while (item != NULL) {
        if (item->id == value_id) {
            return item->value;
        }
        item = item->next;
    }
    return NULL;
}

Id find_value_id(const ValueContainer* const value_container, const char* const value)
{
    ValueItem* item;

    item = value_container->values;
    while (item != NULL) {
        if (strcmp(item->value, value) == 0) {
            return item->id;
        }
        item = item->next;
    }
    return 0;
}

int count_values(ValueContainer* const value_container)
{
    int count;
    ValueItem* item;

    count = 0;
    item = value_container->values;
    while (item != NULL) {
        ++count;
        item = item->next;
    }
    return count;
}

void clear_values(ValueContainer* const value_container)
{
    ValueItem* item;
    ValueItem* next_item;

    item = value_container->values;
    while (item != NULL) {
        next_item = item->next;
        free(item->value);
        free(item);
        item = next_item;
    }
    value_container->values = NULL;
}
